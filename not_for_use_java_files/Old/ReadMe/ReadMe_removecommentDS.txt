removeCommentDS.java

1) commentSymbol not found, no process done

2) Start with commentSymbol, end with commentSymbol2, no process done

     Eg, /* Hello World */

3) Start with commentSymbol, contains commentSymbol2,
     but does not end with commentSymbol2,
     get the value after commentSymbol2 and output string

    Eg, /* Hello World */ int hi;

4) contains commentSymbol, end with commentSymbol2,
   get the value before commentSymbol2 and output String

   Eg, int hi; /* Hello World */

5) contains commentSymbol, contains commentSymbol,
   get the value before commentSymbol and output String
   get the value after commentSymbol2 and output String

   Eg, int hi; /* Hello World*/ int continue;

6) contains commentSymbol,

  6.1) Starts with commentSymbol only, no process done, proceed to search for commentSymbol2

     Eg, /*

  6.2) Consist of commentSymbol, get all the value before commentSymbol, output String,
     proceed to search for commentSymbol2.
     Eg, int a; /*how are you

  6.3) Read in new line, search for commentSymbol2
  6.3.1) commentSymbol2 found, contains only commentSymbol2, output string
  6.3.2) commentSymbol found, get all the value after commentSymbol2, output string

7) commentSymbol not found, output to string
