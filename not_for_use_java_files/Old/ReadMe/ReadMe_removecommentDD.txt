removeCommentDD.java

How it work:

1) input line is empty, no process will be done

2) commentSymbol is after (/*), output string

   Eg, /* //how are you....

3) commentSymbol is before (*/), output string

   Eg, //how are you ... */

4) commentSymbol in between (""), output string

  Eg, "//"

5) Starts with commentSymbol, no process will be done

   5.1) commentSymbol found after the starting position of the string,
        get the string before (//) and Output

6) commentSymbol not found, it will be ignore and output
