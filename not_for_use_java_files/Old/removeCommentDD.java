import java.io.*;
import java.lang.String;

/* Remove "//" comment from C++
*
*  removeCommentDD.start() to use it
*
*  Last Modified : 2017/03/18
*/

public class removeCommentDD{

          //Declaring Variables
          static FileInputStream getFile;
          static PrintWriter printFile;

          static DataInputStream getLine;
          static BufferedReader readIn;

          static int positionofOccurance;
          static String commentSymbol = "//";
          static String tempHold;

          public static void start() throws IOException{

              //Initialize Variables
              getFile = new FileInputStream(".//test/test.cpp");
              printFile = new PrintWriter(new FileWriter(".//test/formatted.cpp"));
              getLine = new DataInputStream(getFile);
              readIn = new BufferedReader(new InputStreamReader(getLine));

              while((tempHold=readIn.readLine())!=null){  //Read in file

                    tempHold =tempHold.trim();            // Remove unncessary spaces

                    if(tempHold.isEmpty()){
                        // if string is empty, execute this
                    }
                    else if ((tempHold.contains("/*") && tempHold.contains(commentSymbol))
                              && (tempHold.indexOf("/*")<tempHold.indexOf(commentSymbol))){
                        //if // is after /*, ignore
                        printFile.println(tempHold);
                    }
                    else if ((tempHold.contains("*/") && tempHold.contains(commentSymbol))
                              && (tempHold.indexOf("*/")>tempHold.indexOf(commentSymbol))){
                        //if // is before */ ignore
                        printFile.println(tempHold);
                    }
                    else if((tempHold.contains("\"") && tempHold.contains(commentSymbol))
                            && ((tempHold.indexOf("\"")<tempHold.indexOf(commentSymbol))
                            && (tempHold.lastIndexOf("\"")>tempHold.indexOf(commentSymbol)))){

                            //if //  is within " ", ignore
                            printFile.println(tempHold);
                    }
                    else if(tempHold.contains(commentSymbol)){ //if // symbol found, execute this
                            if(tempHold.startsWith(commentSymbol)){ // if line starts with //, execute this
                              //Does Nothing
                              }
                            else{ //if // is within the string, execute this
                                positionofOccurance=tempHold.indexOf(commentSymbol); // Get position of symbol
                                tempHold=tempHold.substring(0, positionofOccurance); // Get string before the symbol
                                printFile.println(tempHold); //Ouput to file
                              }
                            }
                    else{ // if symbol not found, execute this
                          printFile.println(tempHold); //Output to file
                        }
                }

              //Close all stream
              readIn.close();
              getFile.close();
              printFile.close();
              getLine.close();
         }
       }
