#include <iostream>
using namespace std;

int main () {
   // Local variable declaration:
   int a = 10;
   int b=0;

   // do loop execution
   LOOP:
   do {
      if( a == 15) {
         // skip the iteration.
         a = a + 1;
         goto LOOP2;
      }

      cout << "value of a: " << a << endl;
      a = a + 1;
   }while( a < 20 );
   LOOP2:{
     cout<<"13"<<endl;
     a=10;
     if(b!=1){
       b+=1;
       goto LOOP;
     }
   }
   cout<<"endl"<<endl;
   return 0;
}
