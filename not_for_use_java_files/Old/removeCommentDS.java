import java.io.*;

/* Remove "/*" comment from C++
*
*  removeCommentDD.start() to use it
*
*  Last Modified : 2017/03/18
*/

public class removeCommentDS{

          //Declaring Variables
          static FileInputStream getFile;
          static PrintWriter printFile;
          static DataInputStream getLine;
          static BufferedReader readIn;

          static String commentSymbol = "/*";
          static String commentSymbol2="*/";
          static String tempHold;

          static int positionofOccurance;
          static int strLen;
          static  int strLen2=commentSymbol2.length();
          static boolean searchNow = true;

          public static void start() throws IOException{

              //Initialize Variables
              getFile = new FileInputStream(".//test/formatted.cpp");
              printFile = new PrintWriter(new FileWriter(".//test/formatted2.cpp"));
              getLine = new DataInputStream(getFile);
              readIn = new BufferedReader(new InputStreamReader(getLine));

               while((tempHold=readIn.readLine())!=null){

                    tempHold=tempHold.trim();

             /*1)*/ if(tempHold.isEmpty()){
                    }
            /*2)*/  else if(tempHold.startsWith(commentSymbol) && tempHold.endsWith(commentSymbol2)){ // Start /*   end */

                            }
            /*3)*/ else if (tempHold.startsWith(commentSymbol) && tempHold.contains(commentSymbol2)){
                                 positionofOccurance=tempHold.indexOf(commentSymbol2)+2;
                                 tempHold=tempHold.substring(positionofOccurance);
                                 tempHold=tempHold.trim();
                                 printFile.println(tempHold);
                                }
            /*4)*/ else if (tempHold.contains(commentSymbol) && tempHold.endsWith(commentSymbol2)){
                                 positionofOccurance=tempHold.indexOf(commentSymbol);
                                 tempHold=tempHold.substring(0,positionofOccurance);
                                 tempHold=tempHold.trim();
                                 printFile.println(tempHold);
                                }
            /*5*/  else if (tempHold.contains(commentSymbol) && tempHold.contains(commentSymbol2)){
                                 String tempHold2="";
                                 positionofOccurance=tempHold.indexOf(commentSymbol);
                                 tempHold2=tempHold.substring(0,positionofOccurance);
                                 tempHold2=tempHold2.trim();
                                 printFile.println(tempHold2);

                                 positionofOccurance=tempHold.indexOf(commentSymbol2)+2;
                                 tempHold=tempHold.substring(positionofOccurance);
                                 tempHold.trim();
                                 printFile.println(tempHold);


                                }
             /*6*/  else if(tempHold.contains(commentSymbol)){

                         if(tempHold.startsWith(commentSymbol) && tempHold.length()==strLen2){ // Only /*
                                      //Does Nothing
                                    }
                         else {
                              //Get the content before /*
                              positionofOccurance=tempHold.indexOf(commentSymbol);
                              tempHold=tempHold.substring(0,positionofOccurance);
                              tempHold=tempHold.trim();
                              printFile.println(tempHold);
                              System.out.println(positionofOccurance);
                              System.out.println(tempHold);
                              }

                         while(searchNow){
                              tempHold=readIn.readLine();
                              tempHold=tempHold.trim();
                              strLen=tempHold.length();
                              positionofOccurance=tempHold.indexOf(commentSymbol2)+2;

                              if(tempHold.contains(commentSymbol2)){
                                    if(strLen>strLen2){
                                        if(tempHold.endsWith(commentSymbol2)){
                                            searchNow=false;
                                        }
                                        else{
                                                    tempHold=tempHold.substring(positionofOccurance);
                                                    tempHold=tempHold.trim();
                                                    printFile.println(tempHold);
                                                    searchNow=false;
                                                  }
                                      }
                                    }
                                }
                                searchNow=true;
                            }

                    else{
                        printFile.println(tempHold);
                    }
                
              }
                //Close all stream
                readIn.close();
                getFile.close();
                printFile.close();
                getLine.close();
        }
}
