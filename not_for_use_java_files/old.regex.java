import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class regex{

  public static void main(String[] args) throws IOException{


          String fileName="forloop.cpp";
          String tempHold, tempHold2;

          StringBuffer sb= new StringBuffer();
          ArrayList<Integer>fPosition= new ArrayList <Integer>();

          int forPosition;

          tempHold=readfile.byString(fileName);


          int x=0;
        //  System.out.println(fPosition.get(2));
        //  System.out.println(tempHold.charAt(161));
          while(true){
                fPosition=regexPosition(tempHold);
                forPosition=fPosition.get(0);
                tempHold2=sb.append(tempHold.substring(0,forPosition)).append("\n").toString();

                sb=new StringBuffer();
                tempHold=sb.append(tempHold2).append(convertToWhile(tempHold,forPosition)).toString();
                sb=new StringBuffer();
System.out.println(tempHold);
                if(x>1){break;}

                x++;
        }

  }

  // for(int a;a<x;x++)
  public static String convertToWhile(String str2convert, int pos1) throws IOException{

          ArrayList<Character> charArray = new ArrayList<Character>(); // Store file Characters into Single Array
          ArrayList<String> conditions = new ArrayList<String>(); // Store forloop conditions

          boolean getCon=true;  // trigger for get conditions function
          boolean getBrac=true; // trigger for get first open bracket function

          int openBrac=0; // counter for number of open bracket
          int closeBrac=0; // counter for number of close bracket
          int forStartPostion=0; // Store first position of open bracket
          int forEndPosition=0; // Store last postion of close bracket
          int incrementPos=0; // Store postion to place increment

          StringBuffer bs = new StringBuffer();
          String holdLast= new String();  // Store unrelevent string after } close bracket
          String holdWhile=new String();  // Store mutated while loop
          String holdInstruction=new String(); //Store forLoop Instruction
          String returnThis=new String(); // holdWhile append to hold last, then return this

          charArray=readfile.str2ALchar(str2convert); // Assign file string to ArrayList Character

          for(int x=pos1;x<charArray.size();x++){
              if(charArray.get(x)=='{'){
                    openBrac++;
                    if(getBrac){ // Get the first postion of the {
                          forStartPostion=x;
                          getBrac=false;
                          }
                    }
              if(charArray.get(x)=='}'){
                    closeBrac++;
                    }

              if((charArray.get(x)=='(')&& getCon){
                      int countC=0;
                      while(true){
                          x++;
                          if(countC==2){
                                bs.append(charArray.get(x));
                                if(charArray.get(x)==')'){
                                    bs.setLength(bs.length() - 1);
                                    bs.append(';');
                                    conditions.add(bs.toString());
                                    bs=new StringBuffer();
                                    getCon=false;
                                    break;
                                  }
                          }
                          else{
                                bs.append(charArray.get(x));
                                if(charArray.get(x)==';'){
                                    if(countC==1) bs.setLength(bs.length() - 1);
                                    conditions.add(bs.toString());
                                    bs=new StringBuffer();
                                    countC++;
                                    }
                         }
                    }
              }


              if(closeBrac>=1){
                  if((openBrac/closeBrac)==1){
                      incrementPos=x;
                      forEndPosition=x+1;
                      break;
                      }
                  }
          }


          for(int x1=forStartPostion;x1<forEndPosition;x1++){
                  if(x1==incrementPos){
                     char closeBracket = charArray.get(x1);
                     bs.append(conditions.get(2)).append(closeBracket).append("\n");
                     holdInstruction=bs.toString();
                     bs=new StringBuffer();
                     break;
                  }
                  else{
                     bs.append(charArray.get(x1));
                  }
          }

          holdWhile=bs.append(conditions.get(0)).append("\n").append("while(").append(conditions.get(1)).append(")").toString();

          holdLast=str2convert.substring(forEndPosition);

          bs=new StringBuffer();

          returnThis=bs.append(holdWhile).append(holdInstruction).append(holdLast).toString();

          return returnThis;
  }



  public static ArrayList<Integer> regexPosition(String str2Check){

      //s? means ignore space tab etc
      //[^)]* find till )

      String RExpression="\\bfor\\s?\\([^)]*";  // Means find for(;;)
      ArrayList<Integer> returnThis = new ArrayList<Integer>();
      Pattern checkRegex = Pattern.compile(RExpression);
      Matcher regexMatcher = checkRegex.matcher(str2Check);



      while(regexMatcher.find()){
            returnThis.add(regexMatcher.start());
          }

      return returnThis;
    }

    public static boolean regexBool (String str2Check){

      String RExpression="for\\s?\\([^)]*";  // Means find for(;;)
      Pattern checkRegex = Pattern.compile(RExpression);
      Matcher regexMatcher = checkRegex.matcher(str2Check);

      return true;

    }


}
