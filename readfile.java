

import java.io.*;
import java.util.*;

/* Read files
*
*  Read in file, Process it, Return Values
*
*  Return a String : readfile.byString(outputFileName);
*  Return a ArrayList : readfile.byArray(outputFileName);
*
*  Last Modified : 2017/03/23
*/

public class readfile{

      //Read & Return as String
      public static String byString(String inputFileName) throws IOException{

            //Initialize Variables
            StringBuffer sb=new StringBuffer();
            String line=null;
            String returnThis=null;
            BufferedReader readIn;

            readIn=new BufferedReader(new FileReader(inputFileName));

            //Start getting Input
            while((line=readIn.readLine())!=null){
                 line=line.trim();
                 if(line.isEmpty()){}
                 else {
                    sb.append(line).append("\n");
                    }
                  }

            //Return String
            readIn.close();
            returnThis=sb.toString();
            return returnThis;
          }

      //Read & Return each line as ArrayList<String>
      public static ArrayList<String> byArray(String inputFileName) throws IOException{

            String line=null;
            BufferedReader readIn;

            ArrayList<String> returnThis = new ArrayList<String>();

            readIn = new BufferedReader(new FileReader(inputFileName));

            while((line=readIn.readLine())!=null){

                 line=line.trim();
                 if(line.isEmpty()){}
                 else{
                    returnThis.add(line);
                    }
                }
            readIn.close();
            return returnThis;
        }

        public static ArrayList<Character> byCharArray(String inputFileName) throws IOException{

            int charInt;

            ArrayList<Character> returnThis = new ArrayList<Character>();
            FileInputStream readIn;

            readIn=new FileInputStream(inputFileName);

            while((charInt=readIn.read())!=-1){
                  returnThis.add((char)charInt);
            }

            return returnThis;
          }


         public static ArrayList<Character> str2ALchar(String inputString){

                 ArrayList<Character>returnThis=new ArrayList<Character>();

                 for(int x=0;x<inputString.length();x++){
                    returnThis.add(inputString.charAt(x));
                 }

                return returnThis;
         }


      }
