

import java.io.*;

/* Write files
*
*  Take in String, Write to file
*
*  Write as String : writefile.byString(inputString,outputFileName);
*
*  Last Modified : 2017/03/23
*/

public class writefile{

            //Take in String, Write to File
            public static void Now(String inputString, String outputFileName) throws IOException{

                    //Initialize Variables
                    BufferedWriter printFile;

                    printFile = new BufferedWriter(new FileWriter(outputFileName));

                    //Write to File
                    printFile.write(inputString);
                    printFile.close();
            }
}
