

import java.io.*;
import java.util.*;

/* Remove "/*" comment from C++
*
*  Read in file, Process it, Output to file
*  To use it: rc_slashstar.Now(inputFileName,outputFileName);
*
*  Last Modified : 2017/03/23
*/

public class rc_slashstar{

       public static void Now(String inputFileName) throws IOException{

          //Initialize Variables
          String fileString;
          StringBuffer sb=new StringBuffer();
          ArrayList<Character> charArray = new ArrayList<Character>();

          fileString=readfile.byString(inputFileName);

          //Assign String to ArrayList
          for(int i = 0; i<fileString.length(); i++){
                charArray.add(fileString.charAt(i));
                }

          //Navigate and Search
          for(int i2=0;i2<charArray.size();i2++){

                //If quatation mark exist, ignore checks, append to sb
                if(charArray.get(i2)=='\"'){
                      do{
                          sb.append(charArray.get(i2));
                          i2++;
                          if(charArray.get(i2)=='\"'){
                              break;
                            }
                          }while(true);
                        }

                 // if comment symbol exits, check for ending too, then remove it(ignore)
                if(charArray.get(i2)=='/' && charArray.get(i2+1)=='*'){
                      while(true){
                            i2++;
                            if(charArray.get(i2)=='*' && charArray.get(i2+1)=='/'){
                                  i2+=2;
                                  break;
                                  }
                                }
                              }

                 //Append to sb
                 sb.append(charArray.get(i2));
                  }

                 //Output to File
                 fileString=sb.toString();
                 writefile.Now(fileString,inputFileName);
                }
}
