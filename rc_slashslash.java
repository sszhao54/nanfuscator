

import java.io.*;
import java.util.*;

/* Remove "//" comment from C++
*
*  Read in file, Process it, Output to file
*  To use it: rc_slashslash.Now(inputFileName,outputFileName);
*
*  Last Modified : 2017/03/23
*/

public class rc_slashslash{

          public static void Now(String inputFileName) throws IOException{

              //Initialize Variables
              int positionofOccurance;
              String fileString;
              String tempHold;
              String commentSymbol = "//";
              StringBuffer sb=new StringBuffer();
              ArrayList<String>lineArray=new ArrayList<String>();

              //Get Data for processing
              lineArray=readfile.byArray(inputFileName);

              //Start Processing
              for(int countX=0;countX<lineArray.size();countX++){

                    tempHold=lineArray.get(countX);

                    //if // is after /*, ignore & write to StringBuffer
                    if ((tempHold.contains("/*") && tempHold.contains(commentSymbol)) && (tempHold.indexOf("/*")<tempHold.indexOf(commentSymbol))){
                              sb.append(tempHold).append("\n");
                              }

                    //if // is before */ ignore & write to StringBuffer
                    else if ((tempHold.contains("*/") && tempHold.contains(commentSymbol)) && (tempHold.indexOf("*/")>tempHold.indexOf(commentSymbol))){
                              sb.append(tempHold).append("\n");
                              }

                    //if // is between quotation, process, ignore & write to StringBuffer
                    else if((tempHold.contains("\"") && tempHold.contains(commentSymbol)) && ((tempHold.indexOf("\"")<tempHold.indexOf(commentSymbol)) && (tempHold.lastIndexOf("\"")>tempHold.indexOf(commentSymbol)))){

                            ArrayList<Character> charArray = new ArrayList<Character>();

                            for(int i = 0; i<tempHold.length(); i++){

                                    charArray.add(tempHold.charAt(i));
                                    }

                            for(int i2=0;i2<charArray.size();i2++){

                                     if(charArray.get(i2)=='\"'){
                                           do{
                                               sb.append(charArray.get(i2));
                                               i2++;
                                               if(charArray.get(i2)=='\"'){
                                                   break;
                                                   }
                                              }while(true);
                                            }
                                     if(charArray.get(i2)=='/' && charArray.get(i2+1)=='/'){
                                           break;
                                           }
                                    sb.append(charArray.get(i2));
                                    }

                           sb.append("\n");
                          }

                    //Last Condition:
                    //if // symbol found, execute this
                    else if(tempHold.contains(commentSymbol)){

                             // if line starts with //, execute this
                            if(tempHold.startsWith(commentSymbol)){}

                             //if // is within the string, execute this
                            else{
                                positionofOccurance=tempHold.indexOf(commentSymbol); // Get position of symbol
                                tempHold=tempHold.substring(0, positionofOccurance); // Get string before the symbol
                                sb.append(tempHold).append("\n"); //Ouput to file
                                }
                              }

                    // if symbol not found, execute this
                    else{
                          sb.append(tempHold).append("\n"); //Output to file
                        }

                } //End of Process

                //Output to File
                fileString=sb.toString();
                writefile.Now(fileString,inputFileName);
         }
       }
