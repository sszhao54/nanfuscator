import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ob_variablename{

          static ArrayList<String> GVN=new ArrayList<String>();

          public static void now(String inputFileName) throws IOException{

            String fileContent=readfile.byString(inputFileName);
            ArrayList<String> fileLine=new ArrayList<String>(readfile.byArray(inputFileName));

            //Initialize Regex Expression for each test case
            ArrayList<String> RegexExpression = new ArrayList<String>(initRegex());

           for(int countY=0;countY<fileLine.size();countY++){
              for(int countC=0;countC<RegexExpression.size();countC++){
                    if(regexbool(RegexExpression.get(countC),fileLine.get(countY))){
                          if(regexbool("main",fileLine.get(countY))){}
                          else{
                          fileContent=changeNow(RegexExpression.get(countC),fileLine.get(countY),fileContent);
                        }
                        }
                      }
                  }
            writefile.Now(fileContent,inputFileName);
          }

          // int
          public static String changeNow(String regex, String inputLineString, String inputString){

                     String variable2replace = variablevalue(regex,inputLineString);
                     String genNew = nameGen();
                     String returnThis=inputString;
                     System.out.println(variable2replace);
                      if(variable2replace.equals("main")){return returnThis;};
                      Pattern checkRegex=Pattern.compile(variable2replace.trim());
                      Matcher matchRegex=checkRegex.matcher(inputString);

                      returnThis=matchRegex.replaceAll(genNew);

                      return returnThis;

          }

          //float
          public static void variablecase2(){

          }

          //This will return variable name declared by user
          public static String variablevalue(String regex, String inputString){
                        //This function will search from the end occruance of regex, to find variable name
                        ArrayList<Integer> variable_pos = new ArrayList<Integer>(regexPosition(regex, inputString));

                        char hold;
                        String returnThis;
                        StringBuffer sb = new StringBuffer();

                        for(int countX=variable_pos.get(1);countX<inputString.length();countX++){

                              hold=inputString.charAt(countX);

                              if(hold==';'||hold=='='||hold==')'|| hold =='('){
                                break;
                              }
                              else{
                                sb.append(hold);
                              }

                        }

                        returnThis=sb.toString();
                        return returnThis;
          }

          public static ArrayList<Integer> regexPosition(String regexExpression,String stringtoCheck){

                 ArrayList<Integer> returnThis = new ArrayList<Integer>();
                 Pattern checkRegex = Pattern.compile(regexExpression);
                 Matcher regexMatcher = checkRegex.matcher(stringtoCheck);

                 while(regexMatcher.find()){
                         returnThis.add(regexMatcher.start());
                         returnThis.add(regexMatcher.end());
                         }
                     return returnThis;
                 }

          public static Boolean regexbool(String regexExpression, String stringtoCheck){
                 Pattern checkRegex=Pattern.compile(regexExpression);
                 Matcher matchRegex=checkRegex.matcher(stringtoCheck);
                 boolean returnThis=matchRegex.find();//matchRegex.find();
                 return returnThis;
                 }


         public static String nameGen(){

                      final String alphabets ="ODlI";
                      final int betstrLen=alphabets.length();

                      String NameGenerated = new String();
                      Random r = new Random();

                      StringBuffer sb=new StringBuffer();

                      while(true){
                            for(int countX=0;countX<8;countX++){
                                  sb.append(alphabets.charAt(r.nextInt(betstrLen)));
                                  }

                            NameGenerated=sb.toString();

                            if(GVN.contains(NameGenerated)){sb=new StringBuffer();}
                            else{
                                 GVN.add(NameGenerated);
                                 return NameGenerated;
                               }
                       }
        }

          private static ArrayList<String> initRegex(){

                  ArrayList<String>returnThis=new ArrayList<String>();

                  returnThis.add("int\\s+");
                  returnThis.add("float\\s+");
                  returnThis.add("double\\s+");
                  returnThis.add("char\\s+");
                  returnThis.add("string\\s+");
                  return returnThis;
          }

}
