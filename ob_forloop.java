import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ob_forloop{

       public static void now(String inputFileName) throws IOException{

                String fileContent=readfile.byString(inputFileName);
                

                //Initialize Regex Expression for each test case
                ArrayList<String> RegexExpression = new ArrayList<String>(initRegex());

                for(int countC=0;countC<RegexExpression.size();countC++){
                        while(regexbool(RegexExpression.get(countC),fileContent)){
                              if(countC==0){fileContent=forCase0(RegexExpression.get(countC), fileContent);}
                              if(countC==1){fileContent=forCase1(RegexExpression.get(countC), fileContent);}
                              if(countC==2){fileContent=forCase2(RegexExpression.get(countC), fileContent);}
                              if(countC==3){fileContent=forCase3(RegexExpression.get(countC), fileContent);}
                              if(countC==4){fileContent=forCase4(RegexExpression.get(countC), fileContent);}
                              if(countC==5){fileContent=forCase5(RegexExpression.get(countC), fileContent);}
                              if(countC==6){fileContent=forCase6(RegexExpression.get(countC), fileContent);}
                              if(countC==7){fileContent=forCase7(RegexExpression.get(countC), fileContent);}
                              }
                      }

                writefile.Now(fileContent,inputFileName);
       }


       // for(;;)
       public static String forCase0(String regex, String inputString){
                //find this replace with while(true)
                Pattern checkRegex=Pattern.compile(regex);
                Matcher matchRegex=checkRegex.matcher(inputString);

                String value2replace="while(true)";
                String returnThis=new String();

                returnThis=matchRegex.replaceAll(value2replace).toString();
                return returnThis;
       }

       //for(int a;;)
       public static String forCase1(String regex, String inputString){

                //find this replace with while(true) with conditions outside
                ArrayList<String> conditions = new ArrayList<String>(regexForLoop(regex,inputString));
                ArrayList<Integer> forStartEndPos=new ArrayList<Integer>(regexPosition(regex, inputString));
                ArrayList<Integer> opencloseBracPos= new ArrayList<Integer>(bracketPosition(forStartEndPos.get(0), inputString));

                StringBuffer sb= new StringBuffer();
                String returnThis=new String();

                String forCondition=conditions.get(0);
                String initValue=sb.append(forCondition.substring(forCondition.indexOf('(')+1,forCondition.indexOf(';'))).append(";").toString().trim();
                String conditionsValue="while(true)";

                //Insert initValue value
                sb=new StringBuffer(inputString);
                sb.insert(forStartEndPos.get(0)-1,initValue);

                //Replace forloop to while(true)
                String input2transform = sb.toString();

                Pattern checkRegex=Pattern.compile(regex);
                Matcher matchRegex=checkRegex.matcher(input2transform);

                returnThis=matchRegex.replaceAll(conditionsValue).toString();

                return returnThis;
        }

         //for(;a<b;)
       public static String forCase2(String regex, String inputString){
                //find this replace with while(a<b)
                ArrayList<String> conditions = new ArrayList<String>(regexForLoop(regex,inputString));

                Pattern checkRegex=Pattern.compile(regex);
                Matcher matchRegex=checkRegex.matcher(inputString);

                StringBuffer sb= new StringBuffer();
                String returnThis=new String();
                String forCondition=conditions.get(0);

                String value2replace=sb.append("while(").append(forCondition.substring(forCondition.indexOf(';')+1,forCondition.lastIndexOf(';'))).append(")").toString().trim();;

                returnThis=matchRegex.replaceAll(value2replace).toString();
                return returnThis;
       }

       //for(;;a++)
       public static String forCase3(String regex, String inputString){
                //replace for(;;a++) with while(true) and move incrementValue to desinated position a++}

                ArrayList<String> conditions = new ArrayList<String>(regexForLoop(regex,inputString));
                ArrayList<Integer> forStartEndPos=new ArrayList<Integer>(regexPosition(regex, inputString));
                ArrayList<Integer> opencloseBracPos= new ArrayList<Integer>(bracketPosition(forStartEndPos.get(0), inputString));

                //String before for(inta;;) and string after }
                String forCondition=conditions.get(0);

                StringBuffer sb= new StringBuffer();
                String returnThis=new String();

                //Get increment value a++
                String incrementValue=sb.append(forCondition.substring(forCondition.lastIndexOf(';')+1,forCondition.lastIndexOf(')'))).append(";").toString().trim();

                //Insert increment value
                sb=new StringBuffer(inputString);
                sb.insert(opencloseBracPos.get(1)-1,incrementValue);

                //Replace forloop to while(true)
                String input2transform = sb.toString();

                Pattern checkRegex=Pattern.compile(regex);
                Matcher matchRegex=checkRegex.matcher(input2transform);

                String value2replace="while(true)";
                returnThis=matchRegex.replaceAll(value2replace).toString();

                return returnThis;
              }

       //For(int a; a<x;  ) confirmed working, no collision
       public static String forCase4(String regex, String inputString){
                //For(int a; a<x;  ) move int a; out of the loop, replace for loop to while(a<x)
                ArrayList<String> conditions = new ArrayList<String>(regexForLoop(regex,inputString));
                ArrayList<Integer> forStartEndPos=new ArrayList<Integer>(regexPosition(regex, inputString));
                ArrayList<Integer> opencloseBracPos= new ArrayList<Integer>(bracketPosition(forStartEndPos.get(0), inputString));

                StringBuffer sb= new StringBuffer();
                String returnThis=new String();


                String forCondition=conditions.get(0);

                String initValue=sb.append(forCondition.substring(forCondition.indexOf('(')+1,forCondition.indexOf(';'))).append(";").toString().trim();
                sb=new StringBuffer();
                String conditionsValue=sb.append("while(").append(forCondition.substring(forCondition.indexOf(';')+1,forCondition.lastIndexOf(';'))).append(")").toString().trim();

                //Insert initValue value
                sb=new StringBuffer(inputString);
                sb.insert(forStartEndPos.get(0)-1,initValue);

                //Replace forloop to while(true)
                String input2transform = sb.toString();

                Pattern checkRegex=Pattern.compile(regex);
                Matcher matchRegex=checkRegex.matcher(input2transform);

                returnThis=matchRegex.replaceAll(conditionsValue).toString();

                return returnThis;
       }

       //for(ina;;a++) confirmed working, no collision
       public static String forCase5(String regex, String inputString){

              ArrayList<String> conditions = new ArrayList<String>(regexForLoop(regex,inputString));
              ArrayList<Integer> forStartEndPos=new ArrayList<Integer>(regexPosition(regex, inputString));
              ArrayList<Integer> opencloseBracPos= new ArrayList<Integer>(bracketPosition(forStartEndPos.get(0), inputString));

              StringBuffer sb= new StringBuffer();
              String returnThis=new String();


              String forCondition=conditions.get(0);

              String initValue=sb.append(forCondition.substring(forCondition.indexOf('(')+1,forCondition.indexOf(';'))).append(";").toString().trim();
              sb=new StringBuffer();
              String incrementValue=sb.append(forCondition.substring(forCondition.lastIndexOf(';')+1,forCondition.lastIndexOf(')'))).append(";").toString().trim();

              //Insert incrementValue, then initValue value
              sb=new StringBuffer(inputString);
              sb.insert(opencloseBracPos.get(1)-1,incrementValue);
              sb.insert(forStartEndPos.get(0)-1,initValue);

              //Replace forloop to while(true)
              String input2transform = sb.toString();

              Pattern checkRegex=Pattern.compile(regex);
              Matcher matchRegex=checkRegex.matcher(input2transform);
              String value2replace="while(true)";
              returnThis=matchRegex.replaceAll(value2replace).toString();

              return returnThis;


       }

       //for(;a<x;a++) confirmed working, no collision
       public static String forCase6(String regex, String inputString){
              //for(;a<x;a++)  , move incrementValue, change while to (a<x)
              ArrayList<String> conditions = new ArrayList<String>(regexForLoop(regex,inputString));
              ArrayList<Integer> forStartEndPos=new ArrayList<Integer>(regexPosition(regex, inputString));
              ArrayList<Integer> opencloseBracPos= new ArrayList<Integer>(bracketPosition(forStartEndPos.get(0), inputString));

              StringBuffer sb= new StringBuffer();
              String returnThis=new String();


              String forCondition=conditions.get(0);


              String conditionsValue=sb.append("while(").append(forCondition.substring(forCondition.indexOf(';')+1,forCondition.lastIndexOf(';'))).append(")").toString().trim();
              sb=new StringBuffer();
              String incrementValue=sb.append(forCondition.substring(forCondition.lastIndexOf(';')+1,forCondition.lastIndexOf(')'))).append(";").toString().trim();


              //Insert incrementValue, then initValue value
              sb=new StringBuffer(inputString);
              sb.insert(opencloseBracPos.get(1)-1,incrementValue);

              //Replace forloop to while(true)
              String input2transform = sb.toString();

              Pattern checkRegex=Pattern.compile(regex);
              Matcher matchRegex=checkRegex.matcher(input2transform);

              returnThis=matchRegex.replaceAll(conditionsValue).toString();

              return returnThis;
       }
       //for(int a;a<b;a++)  confirmed working, no collision
       public static String forCase7(String regex, String inputString){
              // Move initValue out of the loop, move a++ to the }, change to whille(a<b)
              ArrayList<String> conditions = new ArrayList<String>(regexForLoop(regex,inputString));
              ArrayList<Integer> forStartEndPos=new ArrayList<Integer>(regexPosition(regex, inputString));
              ArrayList<Integer> opencloseBracPos= new ArrayList<Integer>(bracketPosition(forStartEndPos.get(0), inputString));

              StringBuffer sb= new StringBuffer();
              String returnThis=new String();

              String forCondition=conditions.get(0);

              String initValue=sb.append(forCondition.substring(forCondition.indexOf('(')+1,forCondition.indexOf(';'))).append(";").toString().trim();
              sb=new StringBuffer();
              String conditionsValue=sb.append("while(").append(forCondition.substring(forCondition.indexOf(';')+1,forCondition.lastIndexOf(';'))).append(")").toString().trim();
              sb=new StringBuffer();
              String incrementValue=sb.append(forCondition.substring(forCondition.lastIndexOf(';')+1,forCondition.lastIndexOf(')'))).append(";").toString().trim();

              //Insert incrementValue, then initValue value
              sb=new StringBuffer(inputString);
              sb.insert(opencloseBracPos.get(1)-1,incrementValue);
              sb.insert(forStartEndPos.get(0)-1,initValue);

              //Replace forloop to while(true)
              String input2transform = sb.toString();

              Pattern checkRegex=Pattern.compile(regex);
              Matcher matchRegex=checkRegex.matcher(input2transform);

              returnThis=matchRegex.replaceAll(conditionsValue).toString();

              return returnThis;

       }


       public static Boolean regexbool(String regexExpression, String stringtoCheck){
              Pattern checkRegex=Pattern.compile(regexExpression);
              Matcher matchRegex=checkRegex.matcher(stringtoCheck);
              boolean returnThis=matchRegex.find();//matchRegex.find();
              return returnThis;
              }

       public static ArrayList<Integer> regexPosition(String regexExpression,String stringtoCheck){

              ArrayList<Integer> returnThis = new ArrayList<Integer>();
              Pattern checkRegex = Pattern.compile(regexExpression);
              Matcher regexMatcher = checkRegex.matcher(stringtoCheck);

              while(regexMatcher.find()){
                      returnThis.add(regexMatcher.start());
                      returnThis.add(regexMatcher.end());
                      }
                  return returnThis;
              }

        public static ArrayList<String> regexForLoop(String regexExpression,String stringtoCheck){

                     ArrayList<String> returnThis = new ArrayList<String>();
                     Pattern checkRegex = Pattern.compile(regexExpression);
                     Matcher regexMatcher = checkRegex.matcher(stringtoCheck);

                     while(regexMatcher.find()){
                             returnThis.add(regexMatcher.group());
                             }
                         return returnThis;
                     }


        public static ArrayList<Integer> bracketPosition(int inForPosition, String stringtoCheck){

              ArrayList<Integer> returnThis=new ArrayList<Integer>();
              ArrayList<Character> charArray=new ArrayList<Character>(readfile.str2ALchar(stringtoCheck));

              int countOpen=0;
              int countClose=0;

              boolean getfirstBracket=true;

              for(int pos=inForPosition;pos<stringtoCheck.length();pos++){

                          if(charArray.get(pos)=='{'){
                               if(getfirstBracket){returnThis.add(pos);getfirstBracket=false;}
                               countOpen++;
                          }
                          if(charArray.get(pos)=='}'){
                               countClose++;
                          }

                          if(countClose>0){
                              if((countClose/countOpen)==1){
                                  returnThis.add(pos);
                                  break;
                              }
                          }
              }
              return returnThis;

        }

       private static ArrayList<String> initRegex(){

               ArrayList<String> returnThis=new ArrayList<String>();

               // for(;;) confirmed working
               returnThis.add("for\\s*\\(\\s*;\\s*;\\s*\\)");
               //for(int a=0;;) confirmed working, no collision
               returnThis.add("for\\s*\\(\\s*[\\!-\\:\\<-\\}]+\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*;\\s*;\\s*\\)");
               //for(;a<b;) confirmed wokring. no collision
               returnThis.add("for\\s*\\(\\s*;\\s*[\\!-\\:\\<-\\}]+\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*;\\s*\\)");
               //for(;;a++) confirmed working, no collision
               returnThis.add("for\\s*\\(\\s*;\\s*;\\s*[A-z\\d\\+\\-]+\\s*\\)");
               //For(int a; a<x;) confirmed working, no collision
               returnThis.add("for\\s*\\(\\s*[\\!-\\:\\<-\\}]+\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*;\\s*[\\!-\\:\\<-\\}]+\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*;\\s*\\)");
               //for(ina;;a++) confirmed working, no collision
               returnThis.add("for\\s*\\(\\s*[\\!-\\:\\<-\\}]+\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]\\s*;\\s*;\\s*[A-z\\d\\+\\-]+\\s*\\)");
               //for(;a<a;a++) confirmed working, no collision
               returnThis.add("for\\s*\\(\\s*;\\s*[\\!-\\:\\<-\\}]+\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*;\\s*[A-z\\d\\+\\-]+\\s*\\)");
               //for(int a;a<b;a++)  confirmed working, no collision
               returnThis.add("for\\s*\\(\\s*[\\!-\\:\\<-\\}]+\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*;\\s*[\\!-\\:\\<-\\}]+\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*[\\!-\\:\\<-\\}]*\\s*;\\s*[A-z\\d\\+\\-]+\\s*\\)");

                return returnThis;
       }
}
